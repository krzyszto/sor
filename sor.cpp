#include <stdio.h>

const unsigned long long int MODULO = 1000000000;
const int P = 1;
const int L = 0;
long long unsigned int N;

int main() {
	scanf("%llu",&N);
	long long unsigned int wyniki[N][N][2];
	long long unsigned int A[N];
	for(long long unsigned int i = 0; i < N; i++) {
		wyniki[i][i][L] = 1;
		wyniki[i][i][P] = 0;
		scanf("%llu", &A[i]);
	}
	for(long long unsigned int i = 2; i <= N; i++) {
		for(long long unsigned int j = 0; j < (N - i + 1); j++) {
			long long unsigned int k = j + i - 1;
			wyniki[j][k][L] = 0;
			wyniki[j][k][P] = 0;
			if (A[j] < A[j+1])
				wyniki[j][k][L] = wyniki[j+1][k][L];
			if (A[k] > A[k-1])
				wyniki[j][k][P] = wyniki[j][k-1][P];
			if (A[j] < A[k])
				wyniki[j][k][L] += wyniki[j+1][k][P];
			if (A[k] > A[j])
				wyniki[j][k][P] += wyniki[j][k-1][L];
			wyniki[j][k][L] %= MODULO;
			wyniki[j][k][P] %= MODULO;
		}
	}
	printf("%llu\n",(wyniki[0][N-1][L] + wyniki[0][N-1][P]) % MODULO);
}
