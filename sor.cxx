#include <stdio.h>

const unsigned long long int MODULO = 1000000000;
const int P = 1;
const int L = 0;
long long unsigned int N;

int main() {
	scanf("%llu",&N);
	long long unsigned int wyniki[N][N][2];
	long long unsigned int A[N];
	for(long long unsigned int i = 0; i < N; i++) {
		wyniki[i][i][L] = 1;
		wyniki[i][i][P] = 0;
		scanf("%llu", &A[i]);
	}
	for(long long unsigned int l = 2; l <= N; l++) {
		for(long long unsigned int i = 0; i < (N - l + 1); i++) {
			long long unsigned int j = i + l - 1;
			wyniki[i][j][L] = 0;
			if (A[i] < A[i+1])
				wyniki[i][j][L] = wyniki[i+1][j][L];
			if (A[i] < A[j])
				wyniki[i][j][L] += wyniki[i+1][j][P];
			wyniki[i][j][L] %= MODULO;
			wyniki[i][j][P] = 0;
			if (A[j] > A[j-1])
				wyniki[i][j][P] = wyniki[i][j-1][P];
			if (A[j] > A[i])
				wyniki[i][j][P] += wyniki[i][j-1][L];
			wyniki[i][j][P] %= MODULO;
		}
	}
	printf("%llu\n",(wyniki[0][N-1][L] + wyniki[0][N-1][P]) % MODULO);
}
